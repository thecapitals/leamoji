# CoreML resources

*Introducton to CoreML: Building a Simple Image Recognition App* : https://appcoda.com/coreml-introduction/

*CoreML Documentation Apple* : https://developer.apple.com/documentation/coreml

*What's New in CoreML Part 1, WWDC 2018* : https://developer.apple.com/videos/play/wwdc2018/708/

*What's New in CoreML Part 2, WWDC 2018* : https://developer.apple.com/videos/play/wwdc2018/709

*Integrating a CoreML Model into Your App* : https://developer.apple.com/documentation/coreml/integrating_a_core_ml_model_into_your_app

*Core ML and Vision: Machine Learning in iOS 11 Tutorial* : https://www.raywenderlich.com/577-core-ml-and-vision-machine-learning-in-ios-11-tutorial

*Getting Started with Core ML — ML on iOS* : https://medium.com/@hunter.ley.ward/ml-on-ios-running-coreml-on-ios-f9cb340f3855

*List of CoreML Models* : https://github.com/likedan/Awesome-CoreML-Models
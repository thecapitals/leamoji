# Machine Learning notes

## What is machine learning?
Machine learning is a field of artificial intelligence that uses statistical techniques to give computer systems the ability to “learn” from data, without being explicitly programmed. Machine learning explores the study and construction of algorithms that can learn from and make predictions on data, through building a model from sample inputs. 

Machine Learning is the science of getting computers to learn and act like humans do, and improve their learning over time in autonomous fashion, by feeding them data and information in the form of observations and real-world interactions.


## Machine learning tasks
are typically classified into several broad categories:

* Supervised learning: The computer is presented with example inputs and their desired outputs, given by a "teacher", and the goal is to learn a general rule that maps inputs to outputs. 

* Semi-supervised learning: The computer is given only an incomplete training signal: a training set with some (often many) of the target outputs missing.

* Active learning: The computer can only obtain training labels for a limited set of instances (based on a budget), and also has to optimize its choice of objects to acquire labels for. 

* Unsupervised learning: No labels are given to the learning algorithm, leaving it on its own to find structure in its input. Unsupervised learning can be a goal in itself (discovering hidden patterns in data).

* Reinforcement learning: Data (in form of rewards and punishments) are given only as feedback to the program's actions in a dynamic environment, such as driving a vehicle or playing a game against an opponent.

## Categorization of machine learning
tasks arises when one considers the desired output of a machine-learned system:
* In classification, inputs are divided into two or more classes, and the learner must produce a model that assigns unseen inputs to one or more of these classes. Spam filtering is an example of classification, where the inputs are email (or other) messages and the classes are "spam" and "not spam".

* In regression the outputs are continuous rather than discrete.

* In clustering, a set of inputs is to be divided into groups. Unlike in classification, the groups are not known beforehand, making this typically an unsupervised task.

* Density estimation finds the distribution of inputs in some space.

* Dimensionality reduction simplifies inputs by mapping them into a lower-dimensional space.

* Topic modeling is a related problem, where a program is given a list of human language documents and is tasked to find out which documents cover similar topics.


## Machine learning algorithms
Regardless of learning style or function, all combinations of machine learning algorithms consist of the following:
* Representation (a set of classifiers or the language that a computer understands)

* Evaluation (aka objective/scoring function)

* Optimization (search method; often the highest-scoring classifier, for example; there are both off-the-shelf and custom optimization methods used)

Supervised algorithms require a data scientist or data analyst with machine learning skills to provide both input and desired output, in addition to furnishing feedback about the accuracy of predictions during algorithm training. Data scientists determine which variables, or features, the model should analyze and use to develop predictions. Once training is complete, the algorithm will apply what was learned to new data.

Unsupervised algorithms do not need to be trained with desired outcome data. Instead, they use an iterative approach called deep learning to review data and arrive at conclusions. Unsupervised learning algorithms -- also called neural networks -- are used for more complex processing tasks than supervised learning systems, including image recognition, speech-to-text and natural language generation.



## Examples of machine learning
Facebook’s News Feed. The News Feed uses machine learning to personalize each member’s feed. 

Financial services
Banks and other businesses in the financial industry use machine learning technology for two key purposes: to identify important insights in data, and prevent fraud. The insights can identify investment opportunities, or help investors know when to trade. Data mining can also identify clients with high-risk profiles, or use cybersurveillance to pinpoint warning signs of fraud.

Government
Government agencies such as public safety and utilities have a particular need for machine learning since they have multiple sources of data that can be mined for insights. Analyzing sensor data, for example, identifies ways to increase efficiency and save money. Machine learning can also help detect fraud and minimize identity theft.

Health care
Machine learning is a fast-growing trend in the health care industry, thanks to the advent of wearable devices and sensors that can use data to assess a patient's health in real time. The technology can also help medical experts analyze data to identify trends or red flags that may lead to improved diagnoses and treatment. 


Marketing and sales
Websites recommending items you might like based on previous purchases are using machine learning to analyze your buying history – and promote other items you'd be interested in. This ability to capture data, analyze it and use it to personalize a shopping experience (or implement a marketing campaign) is the future of retail.

Oil and gas
Finding new energy sources. Analyzing minerals in the ground. Predicting refinery sensor failure. Streamlining oil distribution to make it more efficient and cost-effective. The number of machine learning use cases for this industry is vast – and still expanding.

Transportation
Analyzing data to identify patterns and trends is key to the transportation industry, which relies on making routes more efficient and predicting potential problems to increase profitability. The data analysis and modeling aspects of machine learning are important tools to delivery companies, public transportation and other transportation organizations.


Much of our day-to-day technology is powered by artificial intelligence. Point your camera at the menu during your next trip to Taiwan and the restaurant’s selections will magically appear in English via the Google Translate app.


## Why is machine learning important?
Things like growing volumes and varieties of available data, computational processing that is cheaper and more powerful, and affordable data storage.

All of these things mean it's possible to quickly and automatically produce models that can analyze bigger, more complex data and deliver faster, more accurate results – even on a very large scale. And by building precise models, an organization has a better chance of identifying profitable opportunities – or avoiding unknown risks.





Bronnen: 
https://en.wikipedia.org/wiki/Machine_learning
https://www.techemergence.com/what-is-machine-learning/
https://searchenterpriseai.techtarget.com/definition/machine-learning-ML
https://www.sas.com/en_us/insights/analytics/machine-learning.html#machine-learning-importance


















